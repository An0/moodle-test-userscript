/* ----------------------------------------------------------------------------

 Online Moodle/Elearning/KMOOC test help
 https://greasyfork.org/en/scripts/38999-moodle-elearning-kmooc-test-help

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

 ------------------------------------------------------------------------- */

// ==UserScript==
// @name         Moodle/Elearning/KMOOC test help
// @version      DEVEL
// @description  Online Moodle/Elearning/KMOOC test help
// @author       Yout
// @match        https://elearning.uni-obuda.hu/main/*
// @match        https://elearning.uni-obuda.hu/kmooc/*
// @match        https://qmining.frylabs.net/*
// @match        http://qmining.frylabs.net/*
// @match        file:///*
// @grant        GM_getResourceText
// @grant        GM_info
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_openInTab
// @require      file:///{ELÉRÉSI ÚT IDE}
// @namespace    https://greasyfork.org/users/153067
// ==/UserScript==

(function() {
    console.log('done')
})();
